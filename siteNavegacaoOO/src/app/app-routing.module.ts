import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: "",
    pathMatch: "full",
    redirectTo: "home"
  },
  {
    path: "home",
    loadChildren: () => import("./components/home/home.module")
      .then(m => m.HomeModule)
  },
  {
    path: "cad-imobiliaria",
    loadChildren: () => import("./components/cad-imobiliaria/cad-imobiliaria.module")
    .then(m => m.CadImobiliariaModule)
  },
  {
    path: "cad-imovel",
    loadChildren: () => import("./components/cad-imovel/cad-imovel.module")
    .then(m => m.CadImovelModule)
  },
  {
    path: "cad-locador",
    loadChildren: () => import("./components/cad-locador/cad-locador.module")
    .then(m => m.CadLocadorModule)
  },
  {
    path: "cad-proprietario",
    loadChildren: () => import("./components/cad-proprietario/cad-proprietario.module")
    .then(m => m.CadProprietarioModule)
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
